import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <h1>This is Simple React App!</h1>
        <p>It's really simple.</p>
      </div>
    );
  }
}

export default App;
