module.exports = {
  entry: './main.jsx',
  output: {
    path: './',
    filename: './index.js'
  },
  devServer: {
    inline: true,
    port: 3000,
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        loader: "style!css"
      }
    ]
  }
}
